


#SiNUCA-AV: a sinuca fork to simulate an anti-virus coprocessor.
Refer to **TERMINATOR** paper for more details[1].


Steps involved in the creation of this fork to simulate the AV coprocessor.

Trace generation:
Change OrCS/SiNUCA tracer to add a field for each instruction, where the field identifies whether the instruction belongs to a given image or not.
For the scope of the work, we targetted libc, such that every transition to the libc could then be detected by reading the trace.
In our work, we specifically changed OrCS tracer as it is similar, but more up-to-date with x86 instructions and newer versions of PIN.
We used the SiNUCA simulator due to the more accurate cache hierarchy and directory simulation.


Changes to the simulator:

______
trace_reader.hpp/cpp: 
	
- added code to detect extra field that signals whether the instruction does or does not belong to the libc image.

______
enumerations.hpp/cpp:

- added instruction operation and memory operation types for a specific AV analysis request instruction, which simulates the processor requesting the coprocessor for an AV analysis on a certain transition to libc (i.e. function call).
______
config_examples/: 

- Created a new configuration, where we added an AV instruction cache to emulate the latency of the coprocessor.
- In the configurations, we connect every core either to a different AV cache or to the same AV cache in order to simulate the latencies involved in requesting analysis. The cache mshr buffer works as a request buffer, while the latency emulates the time required by the coprocessor to analyze the function call. This latency can be lowered when we assume that the calls are nonblocking, i.e., the processor does not wait for the coprocessor, but rather, the coprocessor generates an interrupt when it finishes the analysis.
______
sinuca_configurator.cpp: 

- Added port to every processor to connect an AV instruction cache, and code to make the connection and initialization of each AV cache.
______
processor.cpp/hpp:

- added statistics pertaining to number of AV instructions processed (processor.hpp)
- added detection of libc transition (decode stage of processor.cpp)
- when decoding a function call to libc, we check whether we have maximum decode capability (i.e. no uop has been decoded yet). If we do, we generate 4 store instructions, which emulate the processor saving the call parameters to memory, and an AV instruction that is dependent on the 4 stores -- it will be sent once the stores are completed. If we do not have maximum decode capability, we do not decode any other instruction, and wait for the next cycle to perform the decode of the function call to libc.
- added code to treat the instruction operation av as the same case as memory loads in multiple points (rename stage, dispatch stage, execution stage, send_package function, receive_package function, print functions ).
- added code to rename stage to insert AV requests directed to the AV cache to which the processor is connected 
- added specific commit code to increment stats of AV instructions.

______

cache.hpp/cpp:

- added stats (cache.hpp)
- changed initialization (set_tokens function) to detect AV cache.
- changed several points  to treat av instruction the same as a load instruction.
- added specific code to count av statistic and ignore av completed requests. 

_______

directory_controller.cpp:

- added code for treat_cache_request function. In short, if a cache request reaches directory, we solve the request immediately, adding the cache latency to the request and changing the status of the package to ready/xmit where appropriate.
- additional code for directory stats: there should never be any other case for AV requests, so the remainder of the code is just asserts/errors as the only interaction between cache and directory regarding AV memory operations should be in the treat_cache_request function.

