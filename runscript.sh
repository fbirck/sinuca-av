#!/bin/bash

for f in /home/fbm/workspec/5idx*.tid0.stat.out.gz; do
	inp=$(echo "${f%.tid0.stat.out.gz}")
	size=$(echo "70000")
	out=$(echo "${inp}.LAT_${size}_libc5calls.output")
	echo $inp
	echo $out
	./sinuca -config config_examples/sandy_8cores_av${size}/sandy_8cores.cfg -trace ${inp} --result ${out} &
done;
wait
